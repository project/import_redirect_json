<?php

namespace Drupal\import_redirect_json\Service;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Service that manages the redirect import validation and some usable methods.
 */
class RedirectImportJson {
  use StringTranslationTrait;
  
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory service
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  public $messenger;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  public $logger;

  /**
   * Constructs a RedirectImportJson object.
   * 
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    MessengerInterface $messenger,
    LoggerChannelFactoryInterface $logger_factory,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('import_redirect_json.mapping');
    $this->messenger = $messenger;
    $this->logger = $logger_factory->get('import_redirect_json');
  }

  /**
   * Start the redirects import process
   * 
   * @param $fid
   */
  public function importRedirects($fid) {
    $records = $this->getRecordsbyFid($fid);
    $this->deleteTempFile($fid); // Delete the uploaded file
    \Drupal::service('import_redirect_json.batch')->setBatch($records);
  }

  /**
   * Validate JSON file by fid if it has required fields
   * 
   * @param int $fid
   * @return bool
   */
  public function validateFile($fid) {
    $file = $this->loadFile($fid);

    if($records = $this->getFileContent($file->getFileUri())) {
      if($this->validateRequiredFields($records)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Validate records array if it has required mapped fields, set logs if find invalid and set error messages.
   * 
   * @param array @records
   * @return bool
   */
  protected function validateRequiredFields($records) {
    $invalidRecords = [];
    $requiredFields = $this->getRequiredFields();

    foreach ($records as $key => $record) {
      if (count(array_intersect_key(array_flip($requiredFields), $record)) !== count($requiredFields)) {
        $invalidRecords[] = $record;
        $this->logger->error(
          'This object is not according to the mapped fields. Please compare with what you mapped.<br><pre><code>' . print_r($record, TRUE) . '</code></pre>'
        );
      }
    }

    if($count = count($invalidRecords)) {
      $this->messenger->addError($this->t(
        '@count objects are not according the @mapped_fields_route. Please rectify and try again. For more details, please see last @count @log_messages.',
        [
          '@count' => $count,
          '@mapped_fields_route' => Link::fromTextAndUrl($this->t('mapping fields'), Url::fromRoute('import_redirect_json.mapping'))->toString(),
          '@log_messages' => Link::fromTextAndUrl($this->t('log messages'), Url::fromRoute('dblog.overview', [], ['query' => ['type[]' => 'import_redirect_json']]))->toString(),
        ]
      ));

      return FALSE;
    }

    return TRUE;
  }

  /**
   * Load a file by ID
   *
   * @param int $fid
   *   The file ID
   * @return \Drupal\file\FileInterface
   *   File.
   */
  protected function loadFile($fid) {
    /** @var \Drupal\file\Entity\File $file */
    return $this->entityTypeManager->getStorage('file')->load($fid);
  }

  /**
   * Get files content by uri
   * 
   * @param string $uri
   *   The file URI
   * @return array
   */
  protected function getFileContent($uri) {
    if($json = file_get_contents($uri)) {
      if($records = json_decode($json, true)) {
        return $records;
      }
    }
    
    return FALSE;
  }

  /**
   * Get all records in the file by fid
   * 
   * @param int @fid
   *   The file ID
   * @return array
   */
  protected function getRecordsbyFid($fid) {
    $file = $this->loadFile($fid);
    return $this->getFileContent($file->getFileUri());
  }

  /**
   * Get only required fields that will be required to have data in the uplaoded file.
   * 
   * @return array
   */
  protected function getRequiredFields() {
    $fields = [];
    
    foreach ($this->defaultMapping() as $key => $mapping) {
      if($mapping['required']) {
        $fields[] = $this->config->get($key);
      }
    }

    return $fields;
  }

  /**
   * Get all mapped fields
   * 
   * @return array
   */
  public function getMappedFields() {
    $fields = [];
    
    foreach ($this->defaultMapping() as $key => $mapping) {
      $fields[$key] = $this->config->get($key);
    }

    return $fields;
  }

  /**
   * Get fomatted JSON string besed on the mapping
   *
   * @return string
   */
  public function getJsonDummyPattern() {
    $dummayJson = [];

    foreach ($this->defaultMapping() as $key => $value) {
      $dummayJson[$this->config->get($key)] = $value['dummy_value'];
    }

    return json_encode([$dummayJson], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
  }

  /**
   * Default mapping array
   * 
   * @return array
   */
  public function defaultMapping() {
    return [
      'redirect_source__path' => [
        'mapping' => 'source_path',
        'required' => TRUE,
        'label' => 'From',
        'description' => 'Select the key to save in the source/from URL.',
        'dummy_value' => '/from-url',
      ],
      'redirect_redirect__uri' => [
        'mapping' => 'destination',
        'required' => TRUE,
        'label' => 'To',
        'description' => 'Select the key to save in the destination/to URL.',
        'dummy_value' => 'internal:/node/{ID}',
        'default_value' => '/',
      ],
      'status_code' => [
        'mapping' => 'status_code',
        'required' => FALSE,
        'label' => 'Language',
        'description' => 'Select the ket for status code. If not selected, the "301" will be used as default.',
        'dummy_value' => '301',
        'default_value' => '301',
      ],
      'language' => [
        'mapping' => 'language',
        'required' => FALSE,
        'label' => 'Language',
        'description' => 'Select the key for language code. If not selected, the default language will be used.',
        'dummy_value' => 'en',
        'default_value' => \Drupal::languageManager()->getDefaultLanguage()->getId(),
      ],
    ];
  }

  /**
   * Delete the temporary file by ID
   * 
   * @param int @fid
   */
  protected function deleteTempFile($fid) {
    if($file = $this->loadFile($fid)) {
      $file->delete();
    }
  }
}
