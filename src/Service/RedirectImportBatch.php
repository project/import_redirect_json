<?php

namespace Drupal\import_redirect_json\Service;

use Drupal\Core\Database\Connection;
use Drupal\redirect\Entity\Redirect;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\import_redirect_json\Service\RedirectImportJson;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Service that manages the bacth process to import the redirects.
 */
class RedirectImportBatch {
  use StringTranslationTrait;
  use DependencySerializationTrait;

  const BATCH_SIZE = 50;

  /**
   * The redirect import service.
   *
   * @var \Drupal\import_redirect_json\Service\RedirectImportJson
   */
  protected $redirectImport;

  /**
   * Stores database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a RedirectImportJson object.
   * 
   * @param \Drupal\import_redirect_json\Service\RedirectImportJson $redirect_import
   *   The redirect import service
   * @param \Drupal\Core\Database\Connection $database_connection
   *   The database connection.
   */
  public function __construct(RedirectImportJson $redirect_import, Connection $database_connection) {
    $this->redirectImport = $redirect_import;
    $this->connection = $database_connection;
  }

  /**
   * Set up batch process
   * 
   * @param array $records
   */
  public function setBatch($records) {
    // Breakdown process into small batches.
    $operations = [];
    $item_start = 0;

    foreach (array_chunk($records, self::BATCH_SIZE) as $batch_data) {
      $operations[] = [
        [$this, 'batchProcessImport'],
        [$batch_data, $item_start, count($records)],
      ];
      $item_start += self::BATCH_SIZE;
    }

    $batch = [
      'operations' => $operations,
      'title' => $this->t('Import redirects from JSON file'),
      'init_message' => $this->t('Process started.'),
      'progress_message' => $this->t('Importing...'),
      'error_message' => $this->t('An error occurred while exporting redirect entities.'),
      'finished' => [$this, 'batchFinishedImport'],
    ];

    batch_set($batch);
  }

  /**
   * Import redirect per batches.
   *
   * @param array $batch_data
   *   Ids of redirect entities to process.
   * @param int $start
   *   Next item to process.
   * @param int $total
   *   Total of items to process.
   * @param mixed $context
   *   Context array/iterable.
   */
  public function batchProcessImport(array $records, int $start, int $total, &$context) {
    $context['results']['failures'] = isset($context['results']['failures']) ? $context['results']['failures'] : [];
    $context['results']['exists'] = isset($context['results']['exists']) ? $context['results']['exists'] : [];
    
    foreach ($records as $record) {
      try {
        $redirect = $this->createRedirect($record);

        if($redirect === FALSE) {
          $context['results']['exists'][] = $start;
        }
      }
      catch (\Exception $e) {
        $context['results']['failures'][] = $start;
      }

      $start++;
    }

    $context['finished'] = 1;
    if ($start >= $total) {
      $context['results']['processed'] = $start;
    }
    else {
      $context['message'] = $this->t('Importing (@percent%).', [
        '@percent' => (int) (($start / $total) * 100),
      ]);
    }
  }

  /**
   * Finished callback for import batches.
   *
   * @param bool $success
   *   A boolean indicating whether the batch has completed successfully.
   * @param array $results
   *   The value set in $context['results'] by callback_batch_operation().
   * @param array $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   */
  public function batchFinishedImport($success, array $results, array $operations) {
    if ($success) {
      $exists = count($results['exists']);
      $failures = count($results['failures']);      
      $processed = $results['processed'];
      $correct = $processed - $failures - $exists;

      $this->redirectImport->messenger->addStatus($this->t('Import process finished.'));
      $this->redirectImport->messenger->addStatus($this->t(
        'Processed @processed items. Imported: @correct, Failures: @failures, Already existed and ignored: @exists',
        [
          '@processed' => $processed,
          '@correct' => $correct,
          '@failures' => $failures,
          '@exists' => $exists,
        ])
      );
    }
    else {
      $this->redirectImport->messenger->addError($this->t('Import process failed.'));
    }
  }

  /**
   * Create a redirect entity
   * 
   * @param array $record
   * @return bool
   */
  protected function createRedirect($record) {
    $record = $this->organizeRecord($record);
    
    if(!$this->redirectExist($record)) {
      $destination = str_replace('internal:', '', $record['redirect_redirect__uri']);

      $redirect = Redirect::create(['type' => 'redirect']);
      $redirect->setSource($record['redirect_source__path'], []);
      $redirect->setRedirect($destination);
      $redirect->setStatusCode($record['status_code']);
      $redirect->setLanguage($record['language']);
      
      if($redirectSave = $redirect->save()) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Check if redirect already exists
   * 
   * @param array $record
   * @return bool
   */
  protected function redirectExist($record) {
    // Make sure this redirect doesn't already exists.
    $hash = Redirect::generateHash(ltrim($record['redirect_source__path'],'/'), [], $record['language']);

    $query = $this->connection->select('redirect');
    $query->addField('redirect', 'rid');
    $query->condition('hash', $hash, '=');
    $rids = $query->execute()->fetchCol();

    return count($rids) > 0 ? TRUE : FALSE;
  }

  /**
   * Organize $record array for redirect as per mapping
   * 
   * @param array $record
   * @return array
   */
  protected function organizeRecord($record) {
    $defaultmapping = $this->redirectImport->defaultMapping();
    $mappedFields = $this->redirectImport->getMappedFields();

    foreach ($mappedFields as $key => $mappedField) {
      if(isset($record[$mappedField])) {
        $temp = $record[$mappedField];
        unset($record[$mappedField]);
        $record[$key] = $temp;
      }else {
        $record[$key] = $defaultmapping[$key]['default_value'];
      }
    }

    return $record;
  }

}
