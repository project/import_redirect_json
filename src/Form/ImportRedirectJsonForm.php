<?php

namespace Drupal\import_redirect_json\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\import_redirect_json\Service\RedirectImportJson;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to import the redirects from a JSON file.
 */
class ImportRedirectJsonForm extends FormBase {

  /**
   * The redirect import JSON service
   * 
   * @var Drupal\import_redirect_json\Service\RedirectImportJson
   */
  protected $redirectImport;

  /**
   * Constructs a ImportRedirectJson object.
   *
   * @param \Drupal\import_redirect_json\RedirectImportJson $redirect_import_json_service
   *   The factory for configuration objects.
   */
  public function __construct(RedirectImportJson $redirect_import_json_service) {
    $this->redirectImport = $redirect_import_json_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('import_redirect_json'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_redirect_json_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => 'Please upload a JSON file following this pattern to import the redirect data:',
    ];

    $form['expected_format_pre'] = [
      '#type' => 'html_tag',
      '#tag' => 'pre',
      '#value' =>  $this->redirectImport->getJsonDummyPattern(),
    ];

    $form['json_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('JSON File'),
      '#description' => $this->t('The JSON file containing the redirect data in the expected format.'),
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => ['json'],
      ],
      '#upload_location' => 'temporary://import_redirect_json',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import redirects'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();

    if (isset($input['op'])) {
      $fid = reset($form_state->getValue('json_file'));
      if (!$fid) {
        $form_state->setErrorByName('json_file', $this->t('Unable to load the file. Please upload it again.'));
        return;
      }

      if(!$this->redirectImport->validateFile($fid)) {
        $form_state->setErrorByName('json_file', $this->t('Either file is empty or have invalid JSON. Please try again'));
        return;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fid = reset($form_state->getValue('json_file'));
    $this->redirectImport->importRedirects($fid);
  }

}
