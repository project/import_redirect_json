<?php

namespace Drupal\import_redirect_json\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\import_redirect_json\Service\RedirectImportJson;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure mapping to import redirects from JSON file.
 */
class ImportMappingForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'import_redirect_json.mapping';

  /**
   * The redirect import JSON service
   * 
   * @var Drupal\import_redirect_json\Service\RedirectImportJson
   */
  protected $redirectImport;

  /**
   * Constructs a ImportRedirectJson object.
   *
   * @param \Drupal\import_redirect_json\Service\RedirectImportJson $redirect_import
   *   The factory for configuration objects.
   */
  public function __construct(RedirectImportJson $redirect_import) {
    $this->redirectImport = $redirect_import;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('import_redirect_json'),
    );
  }

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_redirect_json_mapping_form';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Map keys from the JSON to import redirects.'),
    ];

    foreach ($this->redirectImport->defaultMapping() as $key => $mapping) {
      $label = $key . ' (' . $mapping['label'] . ')';

      $form[$key] = [
        '#type' => 'textfield',
        '#title' => $this->t($label),
        '#description' => $this->t($mapping['description']),
        '#default_value' => $config->get($key) ? $config->get($key) : $mapping['mapping'],
        '#required' => $mapping['required'],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config(static::SETTINGS);

    foreach (array_keys($this->redirectImport->defaultMapping()) as $mapping) {
      $config->set($mapping, $values[$mapping]);
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }
}
